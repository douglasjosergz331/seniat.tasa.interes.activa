import { Routes } from '@angular/router';
import { TasaInteresActivaComponent } from '../app/componentes/principal/tasa-interes-activa/tasa-interes-activa.component';
import { TasaRoutingModule } from '../app/tasa-routing.module';

export const routes: Routes = [
  ...TasaRoutingModule.routes,
  { path: 'tasa-interes-activa', component: TasaInteresActivaComponent },
  // Otras rutas de tu aplicación, si las tienes
];