import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IncluirService {
  constructor(private http: HttpClient) {}

  consultar(fechaInicio: Date, fechaFin: Date, valor: number) {
    // Aquí puedes realizar la lógica para enviar la solicitud de consulta al servidor
    const params = {
      fecha_inicio: fechaInicio.toISOString(), // Convierte la fecha a formato ISO
      fecha_fin: fechaFin.toISOString(),
      valor: valor.toString() // Convierte el valor numérico a cadena
    };

    return this.http.get('URL_DEL_SERVICIO', { params });
  }
}