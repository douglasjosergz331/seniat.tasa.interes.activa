import { TestBed } from '@angular/core/testing';

import { IncluirService } from './incluir.service';

describe('IncluirService', () => {
  let service: IncluirService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncluirService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
