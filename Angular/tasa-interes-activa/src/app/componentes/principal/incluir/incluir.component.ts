import { Component, OnInit } from '@angular/core';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';

defineLocale('es', esLocale); // Importa el idioma español para ngx-bootstrap

@Component({
  selector: 'app-incluir',
  templateUrl: './incluir.component.html',
  styleUrls: ['./incluir.component.css']
})
export class IncluirComponent implements OnInit {
  fechaInicio: Date | null = null;
  fechaFin: Date | null = null;
  valorTasa: string | null = null;

  ngOnInit(): void {
    this.loadDatePickerStyles();
  }

  loadDatePickerStyles(): void {
    const styleTag = document.createElement('link');
    styleTag.href = 'node_modules/ngx-bootstrap/datepicker/bs-datepicker.css';
    styleTag.rel = 'stylesheet';
    document.head.appendChild(styleTag);
  }

  incluirComponent() {
    if (this.fechaInicio && this.fechaFin && this.valorTasa) {
      console.log('Inclusión exitosa');
    } else {
      console.log('Por favor, completa todos los campos obligatorios');
    }
  }

  obtenerFechaActual(): string {
    return new Date().toISOString().split('T')[0];
  }
}