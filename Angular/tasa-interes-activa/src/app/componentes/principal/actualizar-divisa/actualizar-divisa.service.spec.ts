import { TestBed } from '@angular/core/testing';

import { ActualizarDivisaService } from './actualizar-divisa.service';

describe('ActualizarDivisaService', () => {
  let service: ActualizarDivisaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActualizarDivisaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
