import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActualizarDivisaService {
  constructor(private http: HttpClient) {}

  actualizar(fechaInicio: Date, fechaFin: Date, valorTasa: string) {
    // Aquí puedes realizar la lógica para enviar los datos actualizados al servidor
    const data = {
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
      valorTasa: valorTasa
    };
    return this.http.post('https://localhost:8080/actualizar', data);
  }
}