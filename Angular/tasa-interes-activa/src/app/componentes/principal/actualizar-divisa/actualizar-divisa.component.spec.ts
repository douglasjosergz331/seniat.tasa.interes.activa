import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarDivisaComponent } from './actualizar-divisa.component';

describe('ActualizarDivisaComponent', () => {
  let component: ActualizarDivisaComponent;
  let fixture: ComponentFixture<ActualizarDivisaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActualizarDivisaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActualizarDivisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
