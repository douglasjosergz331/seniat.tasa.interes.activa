import { Component, OnInit } from '@angular/core';
import { ActualizarDivisaService } from '../services/actualizar-divisa.service';

@Component({
  selector: 'app-actualizar-divisa',
  templateUrl: './actualizar-divisa.component.html',
  styleUrls: ['./actualizar-divisa.component.css']
})
export class ActualizarDivisaComponent implements OnInit {
  fechaInicio: Date | null = null;
  fechaFin: Date | null = null;
  valorTasa: string | null = null;

  constructor(private actualizarDivisaService: ActualizarDivisaService) {}

  ngOnInit(): void {
    this.loadDatePickerStyles();
  }

  loadDatePickerStyles(): void {
    const styleTag = document.createElement('link');
    styleTag.href = 'node_modules/ngx-bootstrap/datepicker/bs-datepicker.css';
    styleTag.rel = 'stylesheet';
    document.head.appendChild(styleTag);
  }

  actualizarDivisa() {
    if (this.fechaInicio && this.fechaFin && this.valorTasa) {
      this.actualizarDivisaService.actualizar(this.fechaInicio, this.fechaFin, this.valorTasa)
        .subscribe(() => {
          console.log('Actualización exitosa');
          // Realizar cualquier otra acción necesaria después de la actualización
        });
    } else {
      console.log('Por favor, completa todos los campos obligatorios');
    }
  }

  obtenerFechaActual(): string {
    return new Date().toISOString().split('T')[0];
  }
}