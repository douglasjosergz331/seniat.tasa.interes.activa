import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnularDivisaComponent } from './anular-divisa.component';

describe('AnularDivisaComponent', () => {
  let component: AnularDivisaComponent;
  let fixture: ComponentFixture<AnularDivisaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AnularDivisaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnularDivisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
