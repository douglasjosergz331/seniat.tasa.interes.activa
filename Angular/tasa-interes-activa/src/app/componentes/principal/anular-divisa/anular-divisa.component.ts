import { Component, OnInit } from '@angular/core';
import { AnularDivisaService } from '../services/anular-divisa.service';

@Component({
  selector: 'app-anular-divisa',
  templateUrl: './anular-divisa.component.html',
  styleUrls: ['./anular-divisa.component.css']
})
export class AnularDivisaComponent implements OnInit {
  fechaInicio: Date | null = null;
  fechaFin: Date | null = null;

  constructor(private anularDivisaService: AnularDivisaService) {}

  ngOnInit(): void {}

  anularDivisa() {
    if (this.fechaInicio && this.fechaFin) {
      this.anularDivisaService.anular(this.fechaInicio, this.fechaFin)
        .subscribe(() => {
          console.log('Anulación exitosa');
          // Realizar cualquier otra acción necesaria después de la anulación
        });
    } else {
      console.log('Por favor, completa todos los campos obligatorios');
    }
  }

  obtenerFechaActual(): string {
    return new Date().toISOString().split('T')[0];
  }
}