import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnularDivisaService {
  constructor(private http: HttpClient) {}

  anular(fechaInicio: Date, fechaFin: Date): Observable<any> {
    // Aquí puedes realizar la lógica para enviar la solicitud de anulación al servidor
    const data = {
      fechaInicio: fechaInicio,
      fechaFin: fechaFin
    };
    return this.http.post('https://localhost:8080/anular', data);
  }
}