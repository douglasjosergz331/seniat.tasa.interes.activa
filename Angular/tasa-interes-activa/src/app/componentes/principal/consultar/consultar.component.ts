import { Component } from '@angular/core';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent {
  fechaInicio: Date | null = null;
  fechaFin: Date | null = null;

  consultarDivisa() {
    // Validar los campos obligatorios y realizar la lógica de consulta
    if (this.fechaInicio && this.fechaFin) {
      // Realizar la consulta en la base de datos o en el almacenamiento correspondiente
      console.log('Consulta exitosa');
    } else {
      console.log('Por favor, completa todos los campos obligatorios');
    }
  }
}