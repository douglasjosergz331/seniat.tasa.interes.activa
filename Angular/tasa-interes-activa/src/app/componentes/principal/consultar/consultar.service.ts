import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsultarService {
  constructor(private http: HttpClient) {}

  consultar(fechaInicio: Date, fechaFin: Date) {
    const data = {
      fecha_inicio: fechaInicio.toISOString(), // Convierte la fecha a formato ISO
      fecha_fin: fechaFin.toISOString()
    };

    return this.http.post('https://localhost:8080/consultar', data);
  }
}