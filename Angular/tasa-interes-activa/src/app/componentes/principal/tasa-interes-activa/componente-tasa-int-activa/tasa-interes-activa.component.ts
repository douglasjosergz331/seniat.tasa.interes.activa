import { Component, OnInit } from '@angular/core';
import { TasaInteresActivaService } from './servicio-principal/tasa-interes-activa.service';

@Component({
  selector: 'app-tasa-interes-activa',
  templateUrl: './tasa-interes-activa.component.html',
  styleUrls: ['./tasa-interes-activa.component.css']
})
export class TasaInteresActivaComponent implements OnInit {
  id: number | null = null;
  fechaInicio: Date | null = null;
  fechaFin: Date | null = null;
  valorTasa: number | null = null;
  
  constructor(private tasaInteresActivaService: TasaInteresActivaService) {}

  ngOnInit(): void {}

  guardarTasaInteres() {
    if (this.id && this.fechaInicio && this.fechaFin && this.valorTasa) {
      this.tasaInteresActivaService.guardarTasaInteres(this.id, this.fechaInicio, this.fechaFin, this.valorTasa)
        .subscribe(() => {
          console.log('Tasa de interés guardada exitosamente');
          // Realizar cualquier otra acción necesaria después de guardar la tasa de interés
        });
    } else {
      console.log('Por favor, completa todos los campos obligatorios');
    }
  }

  obtenerFechaActual(): string {
    return new Date().toISOString().split('T')[0];
  }
}