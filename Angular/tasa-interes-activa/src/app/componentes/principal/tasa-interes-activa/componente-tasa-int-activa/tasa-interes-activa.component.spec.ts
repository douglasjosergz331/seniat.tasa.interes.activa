import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasaInteresActivaComponent } from './tasa-interes-activa.component';

describe('TasaInteresActivaComponent', () => {
  let component: TasaInteresActivaComponent;
  let fixture: ComponentFixture<TasaInteresActivaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TasaInteresActivaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TasaInteresActivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
