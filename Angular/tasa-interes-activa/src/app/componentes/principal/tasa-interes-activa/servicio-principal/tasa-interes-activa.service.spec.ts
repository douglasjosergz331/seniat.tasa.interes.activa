import { TestBed } from '@angular/core/testing';

import { TasaInteresActivaService } from './tasa-interes-activa.service';

describe('TasaInteresActivaService', () => {
  let service: TasaInteresActivaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TasaInteresActivaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
