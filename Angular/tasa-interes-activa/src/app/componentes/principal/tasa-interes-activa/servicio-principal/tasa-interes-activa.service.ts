import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TasaInteresActivaService {
  constructor(private http: HttpClient) {}

  mostrarTasaInteres(id: number, fechaInicio: Date, fechaFin: Date, valorUtMonto: number) {
    // Aquí puedes realizar la lógica para enviar los datos de la tasa de interés al servidor
    const data = {
      id: id,
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
      valorUtMonto: valorUtMonto
    };
    return this.http.post('https://localhost:8080', data);
  }
}