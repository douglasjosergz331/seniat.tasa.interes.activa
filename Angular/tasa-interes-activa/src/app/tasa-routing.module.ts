import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TasaInteresActivaComponent } from '../app/componentes/principal/tasa-interes-activa/tasa-interes-activa.component';
import { ConsultarComponent } from '../app/componentes/principal/consultar/consultar.component';
import { IncluirComponent } from '../app/componentes/principal/incluir/incluir.component';
import { ActualizarDivisaComponent } from '../app/componentes/principal/actualizar-divisa/actualizar-divisa.component';
import { AnularDivisaComponent } from '../app/componentes/principal/anular-divisa/anular-divisa.component';

const routes: Routes = [
  { path: 'tasa-interes-activa', component: TasaInteresActivaComponent },
  { path: 'consultar', component: ConsultarComponent },
  { path: 'incluir', component: IncluirComponent },
  { path: 'actualizar-divisa', component: ActualizarDivisaComponent },
  { path: 'anular-divisa', component: AnularDivisaComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasaRoutingModule {
    static routes: any;
}