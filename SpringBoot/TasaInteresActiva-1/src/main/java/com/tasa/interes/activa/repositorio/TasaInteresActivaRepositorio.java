package com.tasa.interes.activa.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TasaInteresActivaRepositorio extends JpaRepository<TasaInteresActivaRepositorio, Long>{

}
