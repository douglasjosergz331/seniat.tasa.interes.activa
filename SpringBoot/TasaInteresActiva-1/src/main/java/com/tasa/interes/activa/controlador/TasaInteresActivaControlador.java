package com.tasa.interes.activa.controlador;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tasa.interes.activa.modelo.TasaInteresActiva;
import com.tasa.interes.activa.repositorio.TasaInteresActivaRepositorio;

@RestController
@RequestMapping("/api/v1/")
public class TasaInteresActivaControlador {
	
	@Autowired
	private TasaInteresActivaRepositorio repositorio;
	
@GetMapping("/interesactiva")
public Collection<TasaInteresActiva> colecccionarTodasLasTasasInteresActivas() {
    Collection<TasaInteresActiva> tasasInteresActivas = repositorio.findAll();
    
    // Realizar operaciones con las tasas de interés activas
    for (TasaInteresActiva tasaInteres : tasasInteresActivas) {
        // Acceder al campo valor de tipo BigDecimal
        BigDecimal valor = tasaInteres.getValor();
        
        // Realizar operaciones con el valor
        // ...
    }
    
    return tasasInteresActivas;
}
	
	@PostMapping("/actualizar")
	public ResponseEntity<TasaInteresActiva> actualizarTasaInteresActiva(@RequestBody TasaInteresActiva tasaInteres) {
		if (tasaInteres.getId() != null) {
			Optional<TasaInteresActiva> tasaExistente = repositorio.findById(tasaInteres.getId());
			if (tasaExistente.isPresent()) {
				TasaInteresActiva tasaActualizada = repositorio.save(tasaInteres);
				return ResponseEntity.ok(tasaActualizada);
			} else {
				return ResponseEntity.notFound().build();
			}
		} else {
			return ResponseEntity.badRequest().build();
		}
	}
	
@PostMapping("/incluirtasa")
public TasaInteresActiva incluirTasaInteresActiva(@RequestBody TasaInteresActiva tasaInteres) {
    // Validar que el valor o monto sea mayor que cero
    if (tasaInteres.getValor() <= 0) {
        throw new IllegalArgumentException("El valor o monto debe ser mayor que cero");
    }
    
    // Validar que la fecha de inicio sea anterior a la fecha de fin
    if (tasaInteres.getFechaInicio().isAfter(tasaInteres.getFechaFin())) {
        throw new IllegalArgumentException("La fecha de inicio debe ser anterior a la fecha de fin");
    }
    
    return repositorio.save(tasaInteres);
}
	
	@GetMapping("/tasa/{id}")
	public ResponseEntity<TasaInteresActiva> consultarTasaInteresActiva(@PathVariable Long id) {
		Optional<TasaInteresActiva> tasaInteres = repositorio.findById(id);
		if (tasaInteres.isPresent()) {
			return ResponseEntity.ok(tasaInteres.get());
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/anulartasa/{id}")
	public ResponseEntity<String> anularTasaInteresActiva(@PathVariable Long id) {
		Optional<TasaInteresActiva> tasaInteres = repositorio.findById(id);
		if (tasaInteres.isPresent()) {
			repositorio.deleteById(id);
			return ResponseEntity.ok("Tasa de interés activa eliminada correctamente");
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}