import { TestBed } from '@angular/core/testing';

import { IncluirTasaService } from './incluir-tasa.service';

describe('IncluirTasaService', () => {
  let service: IncluirTasaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncluirTasaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
