import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirTasaComponent } from './incluir-tasa.component';

describe('IncluirTasaComponent', () => {
  let component: IncluirTasaComponent;
  let fixture: ComponentFixture<IncluirTasaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IncluirTasaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IncluirTasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
