import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IncluirTasaService {
  private apiUrl = 'http://localhost:8080/api/actualizar'; // URL de la API de consultar en Spring Boot

  constructor(private http: HttpClient) { }

  incluirtasa(id: number, fecha_inicio: Date, fecha_fin: Date, valor_tasa: number): Observable<any> {
    const data = {
      id: id,
      fecha_inicio: fechaInicio,
      fecha_fin: fechaFin,
      valor_tasa: valor_tasa
    };
    return this.http.post('https://localhost:8080', data);
  }
}