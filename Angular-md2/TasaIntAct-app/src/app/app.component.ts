import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { ConsularTasaComponent } from './consular-tasa/consular-tasa.component';
import { IncluirTasaComponent } from './incluir-tasa/incluir-tasa.component';


@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [CommonModule, RouterOutlet, ConsularTasaComponent,IncluirTasaComponent]
})
export class AppComponent {
  title = 'TasaInteresActivoDos';  
}
