import { Component } from '@angular/core';

@Component({
  selector: 'app-anular-tasa',
  standalone: true,
  imports: [],
  templateUrl: './anular-tasa.component.html',
  styleUrl: './anular-tasa.component.css'
})
export class AnularTasaComponent {
  rutaImagen: string = 'assets/img/seniat.png';
}
