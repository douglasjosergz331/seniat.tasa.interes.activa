import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnularTasaComponent } from './anular-tasa.component';

describe('AnularTasaComponent', () => {
  let component: AnularTasaComponent;
  let fixture: ComponentFixture<AnularTasaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AnularTasaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnularTasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
