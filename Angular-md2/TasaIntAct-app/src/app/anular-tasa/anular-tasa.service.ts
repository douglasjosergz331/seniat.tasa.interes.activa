import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnularTasaService {
  apiUrl: any;
  constructor(private http: HttpClient) { }

  anulartasa(id: number, fecha_inicio: Date, fecha_fin: Date, valor_tasa: number): Observable<any> {
    const data = {
      id: id,
      fecha_inicio: fecha_inicio,
      fecha_fin: fecha_fin,
      valor_tasa: valor_tasa
    };
    return this.http.post('https://localhost:8080', data);
  }
}