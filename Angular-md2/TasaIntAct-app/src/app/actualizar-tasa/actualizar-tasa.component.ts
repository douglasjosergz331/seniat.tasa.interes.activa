import { Component } from '@angular/core';

@Component({
  selector: 'app-actualizar-tasa',
  standalone: true,
  imports: [],
  templateUrl: './actualizar-tasa.component.html',
  styleUrl: './actualizar-tasa.component.css'
})
export class ActualizarTasaComponent {
  rutaImagen: string = 'assets/img/seniat.png';
}
