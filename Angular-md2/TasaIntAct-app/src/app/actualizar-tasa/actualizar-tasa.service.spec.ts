import { TestBed } from '@angular/core/testing';

import { ActualizarTasaService } from './actualizar-tasa.service';

describe('ActualizarTasaService', () => {
  let service: ActualizarTasaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActualizarTasaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
