import { TestBed } from '@angular/core/testing';

import { ConsularTasaService } from './consular-tasa.service';

describe('ConsularTasaService', () => {
  let service: ConsularTasaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsularTasaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
