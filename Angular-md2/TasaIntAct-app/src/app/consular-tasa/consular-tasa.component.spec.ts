import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsularTasaComponent } from './consular-tasa.component';

describe('ConsularTasaComponent', () => {
  let component: ConsularTasaComponent;
  let fixture: ComponentFixture<ConsularTasaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConsularTasaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConsularTasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
