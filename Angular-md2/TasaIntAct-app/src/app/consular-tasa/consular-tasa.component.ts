import { Component } from '@angular/core';

@Component({
  selector: 'app-consular-tasa',
  standalone: true,
  imports: [],
  templateUrl: './consular-tasa.component.html',
  styleUrl: './consular-tasa.component.css'
})
export class ConsularTasaComponent {
  rutaImagen: string = 'assets/img/seniat.png';

}
